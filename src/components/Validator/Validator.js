import React from 'react';

export default function Validator() {
    return (
        <div className="wrapper">

            <div className="panel__right float-right w-100 pt-5">
                <div className="panel__title h2 font-weight-bold mb-5">
                    Today
                </div>
                <div className="panel__wrapper-inner">
                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--red">!</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">NOW</div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">14:35</div>
                            </div>
                            <div className="panel__time">
                                <div className="font-weight-bold">13.35</div>
                            </div>
                        </div>

                        <div className="panel__description-issue">
                            All good
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">NOW</div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">14:35</div>
                            </div>
                            <div className="panel__time">
                                <div className="font-weight-bold">13.35</div>
                            </div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.40</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.35</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.35</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">07.25</div>
                            </div>
                            <div className="panel__time">
                                <div className="font-weight-bold">00.00</div>
                            </div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>
                    <div className="panel__title h2 font-weight-bold mt-60 mb-5">
                        Yesterday
                    </div>
                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--red">!</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">23.55</div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">14:35</div>
                            </div>
                            <div className="panel__time">
                                <div className="font-weight-bold">13.35</div>
                            </div>
                        </div>
                        <div className="panel__description-issue">
                            All good
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">13.15</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">13.35</div>
                            </div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.40</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.35</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <span className="alert__icon alert__icon--orange">?</span>
                        <div className="panel__time">
                            <div className="font-weight-medium">07.35</div>
                        </div>
                        <div className="panel__description-issue">
                            Device restarted
                        </div>
                    </div>

                    <div className="panel__single-info">
                        <div className="font-weight-bold d-flex justify-content-around flex-column">
                            <div className="panel__time">
                                <div className="font-weight-bold">07.25</div>
                            </div>
                            <div className="panel__time">
                                <div className="font-weight-bold">00.00</div>
                            </div>
                        </div>
                        <div className="panel__description-issue">
                            The device is not working
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

