import React from 'react';

import {
    Link
} from "react-router-dom";

export default function Navbar() {
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark d-md-none">
                <div>
                    <Link to="/" className="navbar-brand d-md-none">
                        <img src="img/logo.png" alt="Faro"/>
                    </Link>
                </div>
                <button className="navbar-toggler navbar-toggler-right float-right" type="button" data-toggle="collapse"
                        data-target="#navb">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navb">
                    <ul className="nav menu d-flex justify-content-center" id="menu-rwd">
                        <li className="nav-item"><Link className="nav-link d-flex align-items-center" to="/">
                            <i className="material-icons mr-3">home</i>
                            <span className="nav-item__title">Dashboard</span></Link></li>
                        <li className="nav-link">
                            <Link className="nav-item d-flex align-items-center collapsed has-submenu" to="#submenu1rwd"
                               data-toggle="collapse" data-target="#submenu1rwd"><i
                                className="material-icons mr-3">smartphone</i><span className="nav-item__title">Device
                    Management</span><i
                                className="material-icons arrow-down mr-0 mr-md-3 ml-2 ml-lg-auto">keyboard_arrow_down</i>
                            </Link>
                            <div className="collapse" id="submenu1rwd" data-parent="#menu-rwd">
                                <ul className="flex-column pl-2 nav">
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">All Devices</Link>
                                    </li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">New Devices
                                        <span className="alert-radius mr-0 mr-md-3 ml-2 ml-lg-auto">2</span></Link></li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">Software
                                        update</Link></li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">Device
                                        settings</Link></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-link"><Link className="nav-item d-flex align-items-center collapsed has-submenu"
                                                    to="#submenu2rwd"
                                                    data-toggle="collapse" data-target="#submenu2rwd"><i
                            className="material-icons mr-3">directions_bus</i><span className="nav-item__title">Some
                menu item</span><i
                            className="material-icons arrow-down mr-0 mr-md-3 ml-2 ml-lg-auto">keyboard_arrow_down</i></Link>
                            <div className="collapse" id="submenu2rwd" data-parent="#menu-rwd">
                                <ul className="flex-column pl-2 nav">
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="#">All Devices</Link>
                                    </li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">New Devices</Link>
                                    </li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">Software
                                        update</Link></li>
                                    <li className="nav-link"><Link className="nav-item d-flex align-items-center py-0"
                                                                to="/">Device
                                        settings</Link></li>
                                </ul>
                            </div>
                        </li>
                        <li className="nav-link"><Link className="nav-item d-flex align-items-center has-submenu collapsed"
                                                    to="#submenu3rwd"
                                                    data-toggle="collapse" data-target="#submenu3rwd"> <i
                            className="material-icons mr-3">settings</i>
                            <span className="nav-item__title">Settings</span><i
                                className="material-icons arrow-down mr-0 mr-md-3 ml-2 ml-lg-auto">keyboard_arrow_down</i></Link>
                            <div className="collapse" id="submenu3rwd" data-parent="#menu-rwd">
                                <ul className="flex-column pl-2 nav">
                                    <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                                href="#">All Devices</a>
                                    </li>
                                    <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                                href="#">New Devices</a>
                                    </li>
                                    <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                                href="#">Software
                                        update</a></li>
                                    <li className="nav-link"><a className="nav-item d-flex align-items-center py-0"
                                                                href="#">Device
                                        settings</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <nav className="navbar navbar-expand navbar-light bg-white topbar static-top d-none d-md-flex">
                <div className="navbar__text">
                    <div className="h2">DEVICE MANAGEMENT</div>
                    <div className="breadcrumbs"><Link className="active" to="#">All devices</Link></div>
                </div>

                <ul className="navbar-nav ml-2 ml-lg-auto">

                    <li className="nav-item dropdown no-arrow d-sm-none">
                        <a className="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-search fa-fw h-auto"></i>
                        </a>
                    </li>


                    <li className="nav-item dropdown no-arrow mx-1 align-self-center">
                        <a className="nav-link dropdown-toggle no-arrow" href="#" id="messagesDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="material-icons arrow-down h-auto color-purple mr-3">loop</i>

                        </a>
                    </li>



                    <li className="nav-item dropdown no-arrow align-self-center">
                        <a className="nav-link dropdown-toggle no-arrow" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="material-icons arrow-down h-auto color-purple mr-3">account_circle</i>
                        </a>

                        <div className="dropdown-menu dropdown-menu-right"
                             aria-labelledby="userDropdown">
                            <a className="dropdown-item" href="#">
                                <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a className="dropdown-item" href="#">
                                <i className="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a className="dropdown-item" href="#">
                                <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div className="dropdown-divider"></div>
                            <a className="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
        </React.Fragment>
    );
};

