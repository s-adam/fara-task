import React from 'react';

import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import Dashboard from "../../pages/dashboard";
import AllDevices from "../../pages/errors";
import Warnings from "../../pages/warnings";

export default function App() {
    return (

        <Router>
            <Switch>
                <Route exact path={"/"} component={Dashboard}/>
                <Route exact path={"/all-devices"} component={AllDevices}/>
                <Route exact path={"/platform-validator"} component={Warnings}/>
            </Switch>
        </Router>
    );
};

