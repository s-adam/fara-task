import React from 'react';
import Navbar from '../components/Navbar/Navbar';
import Sidebar from '../components/Sidebar/Sidebar';
import Validator from '../components/Validator/Validator';

// import Sidebar from '';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";
import Errors from "./errors";
const MyComponent = ({ match }) => (
    <div>{console.log(match)}</div>
)
export default function Warnings() {
    return (
        <React.Fragment>
            <Sidebar/>
            <div id="content-wrapper" className="d-flex flex-column w-100">
                <div id="content">
                    <Navbar/>
                    <Validator/>
                </div>
            </div>
        </React.Fragment>
    );
};

