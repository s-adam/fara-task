import React from 'react';
import Navbar from '../components/Navbar/Navbar';
import Sidebar from '../components/Sidebar/Sidebar';

// import Sidebar from '';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";

export default function Dashboard() {
    return (
        <React.Fragment>
            <Sidebar/>
            <div id="content-wrapper" className="d-flex flex-column w-100">

                <div id="content">
                    <Navbar/>
                </div>
            </div>
        </React.Fragment>
    );
};

